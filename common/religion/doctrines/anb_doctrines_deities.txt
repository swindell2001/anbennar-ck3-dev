﻿doctrine_patron_deity = {
	group = "main_group"
	is_available_on_create = {
		religion_tag = cannorian_pantheon_religion
	}
	
	doctrine_patron_deity_castellos = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_castellos }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_castellos = 0
		}
	}
	
	doctrine_patron_deity_the_dame = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_the_dame }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_the_dame = 0
		}
	}
	
	doctrine_patron_deity_esmaryal = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_esmaryal }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_esmaryal = 0
		}
	}
	
	doctrine_patron_deity_falah = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_falah }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_falah = 0
		}
	}
	
	doctrine_patron_deity_nerat = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_nerat }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_nerat = 0
		}
	}
	
	doctrine_patron_deity_adean = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_adean }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_adean = 0
		}
	}
	
	doctrine_patron_deity_ryala = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_ryala }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_ryala = 0
		}
	}
	
	doctrine_patron_deity_balgar = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_balgar }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_balgar = 0 #maybe also for dwarven ancestor worship
		}
	}
	
	doctrine_patron_deity_ara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_ara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_ara = 0
		}
	}
	
	doctrine_patron_deity_munas = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_munas }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_munas = 0
		}
	}
	
	# doctrine_patron_deity_nathalyne = {
		# piety_cost = {
			# value = faith_doctrine_cost_low
			# if = {
				# limit = { has_doctrine = doctrine_patron_deity_nathalyne }
				# multiply = 0
			# }
		# }
		# parameters = {
			# hostility_override_doctrine_patron_deity_nathalyne = 0
		# }
	# }
	
	# doctrine_patron_deity_begga = {
		# piety_cost = {
			# value = faith_doctrine_cost_low
			# if = {
				# limit = { has_doctrine = doctrine_patron_deity_begga }
				# multiply = 0
			# }
		# }
		# parameters = {
			# hostility_override_doctrine_patron_deity_begga = 0
		# }
	# }
	
	doctrine_patron_deity_minara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_minara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_minara = 0
		}
	}
}